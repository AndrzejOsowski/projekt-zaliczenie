This program is a user of the OMDB API.
The program will work as follows: a database will be created (if it no longer exists) and a record will be added to our database if we find a record in the OMDB database.

A few examples of how to run if we are in a script directory:
The database argument is required, the rest of the arguments are not.
The order of giving arguments is not important.

Searching only by title:
python main.py -d 'example.db' -t 'Fast & Furious 6'

Searching by title and year:
python main.py -d 'hello.db' -y 1994 -t 'The Shawshank Redemption'

Searching by title, year and id:
python main.py -i 'tt1905041' -d 'test.db' -t 'Standing Up' -y 2013

Searching only by id:
python main.py -d 'test.db' -i 'tt1905041'