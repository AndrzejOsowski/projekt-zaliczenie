"""
Project: OMDB API CLIENT
Project_link: https://gitlab.com/AndrzejOsowski/projekt-zaliczenie
Author: Osowski Andrzej
License: GNU/GPL
"""

import omdb, sys, sqlite3, argparse

"""ARGUMENTS"""
p = argparse.ArgumentParser()
p.add_argument('-t', help='Title(string)', type=str)
p.add_argument('-i', help='Id(string)', type=str)
p.add_argument('-y', help='Year(int)', type=int)
p.add_argument('-d', help='DATABASE(string)', type=str, required=True)
args = p.parse_args()
""""""


"""OMDB SEARCHING"""
try:
    omdb.set_default('apikey', '4b84cdc9')
    if args.t:
        dict=(omdb.get(title=args.t))
    elif args.i:
        dict=(omdb.imdbid(args.i))
    elif args.t and args.i:
        dict=(omdb.get(title=args.t, imdbid=args.i))
    elif args.t and args.y:
        dict=(omdb.get(title=args.t, year=args.y))
    elif args.i and args.y:
        dict=(omdb.get(year=args.y, imdbid=args.i))
    elif args.i and args.y and args.t:
        dict=(omdb.get(year=args.y, imdbid=args.i, title=args.t))

    title = dict["title"]
    year = dict["year"]
    id = dict["imdb_id"]

    print("THE FOLLOWING RECORD WAS FOUD: " + title +", " + year + ", " + "," + id)
except:
    print("NOT FOUND IN THE DATABASE. RERUN APLICATION WITH OTHER ARGUMENTS")
    sys.exit(0)
""""""



"""SQL"""
try:
    conn = sqlite3.connect(args.d)
    c = conn.cursor()
    c.execute('CREATE TABLE IF NOT EXISTS stuffToPlot(id TEXT, titile TEXT, year REAL)')
    string_to_execute = "INSERT INTO stuffToPlot VALUES(""'" + id + "'"", ""'" + title + "'"", " + year + ")"
    c.execute(string_to_execute)
    conn.commit()
    c.close()
    conn.close()
except:
    print("DATABASE WRITE ERROR. RERUN APLICATION AND TRY AGAIN")
    sys.exit(0)
""""""
